import readline from 'readline';
import { createRequire } from 'module';
const require = createRequire(import.meta.url);

const commands = {
  cat: (await import('./commands/cat.js')).default,
  nano: (await import('./commands/nano.js')).default
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true
});

rl.setPrompt('jsDesk> ');
rl.prompt();

rl.on('line', async (input) => {
  const [command, ...args] = input.trim().split(' ');
  if (commands[command]) {
    await commands[command](args, rl);
  } else {
    console.log(`Command not found: ${command}`);
  }
  rl.prompt();
});
