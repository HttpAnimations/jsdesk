import fs from 'fs';

export default function (args, rl) {
  if (args.length === 0) {
    console.log('Usage: cat <filename>');
    rl.prompt();
    return;
  }
  
  const filename = args[0];
  fs.readFile(filename, 'utf8', (err, data) => {
    if (err) {
      console.error(`Error reading file: ${err.message}`);
    } else {
      console.log(data);
    }
    rl.prompt();
  });
};
