import fs from 'fs';
import readline from 'readline';

export default async function (args, rl) {
  if (args.length === 0) {
    console.log('Usage: nano <filename>');
    rl.prompt();
    return;
  }

  const filename = args[0];
  let content = '';

  try {
    content = await fs.promises.readFile(filename, 'utf8');
  } catch (err) {
    if (err.code !== 'ENOENT') {
      console.error(`Error reading file: ${err.message}`);
      rl.prompt();
      return;
    }
  }

  console.log(`Editing ${filename} (Ctrl+S to save, Ctrl+C to exit)\n`);

  const editorInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
  });

  editorInterface.setPrompt('');
  let lines = content.split('\n');
  editorInterface.write(content);

  editorInterface.on('line', (line) => {
    lines.push(line);
    editorInterface.write('\n');
  });

  process.stdin.on('keypress', async (char, key) => {
    if (key && key.ctrl && key.name === 's') {
      const finalContent = lines.join('\n');
      try {
        await fs.promises.writeFile(filename, finalContent, 'utf8');
        console.log(`\nFile saved: ${filename}`);
      } catch (err) {
        console.error(`Error writing file: ${err.message}`);
      }
      editorInterface.prompt();
    } else if (key && key.ctrl && key.name === 'c') {
      console.log('\nExiting editor...');
      editorInterface.close();
      rl.prompt();
    } else if (char && !key.ctrl) {
      process.stdout.write(char);
      lines[lines.length - 1] += char;
    }
  });

  editorInterface.prompt();
}
